/*
	graphicGraphingJS.js - JavaScript graphing library that make graphical plots
	Copyright (C) 2015	Kyle Johnson - fredrik.johnson.kj@gmail.com

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

 /* todo
sort data

 */

// function to get the extreme values in every direction
function gg_getExtremes(dataObj) {
	var extremes = [null, null, null, null];
	for (var i = 0; i < dataObj.length; i++) {
		// x minimum
		if (extremes[0] == null || dataObj[i][0] < extremes[0]) {
			extremes[0] = dataObj[i][0];
		}
		// x maximum
		if (extremes[1] == null || dataObj[i][0] > extremes[1]) {
			extremes[1] = dataObj[i][0];
		}
		// y minimum
		if (extremes[2] == null || dataObj[i][1] < extremes[2]) {
			extremes[2] = dataObj[i][1];
		}
		// y maximum
		if (extremes[3] == null || dataObj[i][1] > extremes[3]) {
			extremes[3] = dataObj[i][1];
		}
	}
	return extremes;
}

// function to scale the x values
function gg_scaleX(value, min, xRange, canvasWidth, lSpace, rSpace) {
	return (((value - min) * (canvasWidth - lSpace - rSpace)) / xRange) + lSpace;
}

// function to scale the y values
function gg_scaleY(value, min, yRange, canvasHeight, bSpace, flip) {
	var scaled = (((value - min) * (canvasHeight - bSpace)) / yRange) + bSpace;
	if (flip) {
		return scaled;
	}
	else {
		return canvasHeight - scaled;
	}
}

// function to randomize the placement of an image
function gg_shakeValue(value, movement) {
	var amount = Math.random() * movement;
	if (Math.random() > .5) {
		amount *= -1;
	}
	return value + amount;
}

// function to plot the data
function gg_plotData(context, data, canvasSize) {
	// set the default values
	var flip = false;
	var yScale = 0;
	var drawLine = false;
	var fill = false;
	var fillColor = "#888888";
	var leftSpace = 0;
	var bottomSpace = 0;
	// check for changes to the default values
	if ('flip' in data) {
		flip = data['flip'];
	}
	if ('yScale' in data) {
		yScale = data['yScale'];
	}
	if ('drawLine' in data) {
		drawLine = data['drawLine'];
	}
	if ('fill' in data) {
		fill = data['fill'];
	}
	if ('fillColor' in data) {
		fillColor = data['fillColor'];
	}

	var extremes = gg_getExtremes(data['data']);
	var minX = extremes[0];
	var minY = extremes[2];
	var xRange = extremes[1] - extremes[0];
	var yRange = extremes[3] - extremes[2];

	minY = 0;

	if (drawLine) { // draw the line
		var start = true;
		for (var i = 0; i < data['data'].length; i++) {
			var x = gg_scaleX(data['data'][i][0], minX, xRange, canvasSize[0], leftSpace, 0);
			var y = gg_scaleY(data['data'][i][1], minY, yRange + yScale, canvasSize[1], bottomSpace, flip);
			if (start) {
				context.moveTo(x, y);
				start = false;
			}
			context.lineTo(x, y);
			context.stroke();
		}
	}

	if (fill) {
		var start = true;
		var overlapConstant = data['imageObj'].height * .75;
		context.beginPath();
		context.fillStyle = fillColor;
		for (var i = 0; i < data['data'].length; i++) {
			var x = gg_scaleX(data['data'][i][0], minX, xRange, canvasSize[0], leftSpace, 0);
			var y = gg_scaleY(data['data'][i][1], minY, yRange + yScale, canvasSize[1], bottomSpace, flip);
			if (flip) {
				y -= overlapConstant;
			}
			else {
				y += overlapConstant;
			}
			if (start) {
				context.moveTo(x, canvasSize[1]);
				context.lineTo(x, y);
				start = false;
			}
			context.lineTo(x, y);
		}
		context.lineTo(canvasSize[0], canvasSize[1]);
		context.closePath();
		context.fill();
	}

	// fill in the graph with graphics
	if (data['imageLoaded'] && !data['imageLoadError']) {
		var points = [];
		var lineMovement = [.1 * data['imageObj'].width, .1 * data['imageObj'].height];
		var interiorMovement = [.2 * data['imageObj'].width, .2 * data['imageObj'].height];
		for (var i = 0; i < data['data'].length - 1; i++) {
			var xVal = data['data'][i][0];
			var dyDIVdx = (data['data'][i+1][1] - data['data'][i][1]) / (data['data'][i+1][0] - data['data'][i][0]);
			while (xVal < data['data'][i+1][0]) {
				var imgX = gg_scaleX(xVal, minX, xRange, canvasSize[0], leftSpace, 0);
				imgX -= (data['imageObj'].width / 2);
				var yVal = data['data'][i][1] + (xVal - data['data'][i][0]) * dyDIVdx;
				var imgY = gg_scaleY(yVal, minY, yRange + yScale, canvasSize[1], bottomSpace, flip);
				if (flip) {
					imgY -= data['imageObj'].height;
				}
				context.drawImage(data['imageObj'], gg_shakeValue(imgX, lineMovement[0]), gg_shakeValue(imgY, lineMovement[1]));
				//context.drawImage(data['imageObj'], imgX, imgY);
				points.push([imgX, imgY]);
				xVal += data['imageObj'].width;
			}
		}
		// draw the last column
		var imgX = gg_scaleX(data['data'][data['data'].length - 1][0], minX, xRange, canvasSize[0], leftSpace, 0);
		imgX -= (data['imageObj'].width / 2);
		var yVal = data['data'][i][1] + (xVal - data['data'][i][0]) * dyDIVdx;
		var imgY = gg_scaleY(data['data'][data['data'].length - 1][1], minY, yRange + yScale, canvasSize[1], bottomSpace, flip);
		if (flip) {
			imgY -= data['imageObj'].height;
		}
		context.drawImage(data['imageObj'], gg_shakeValue(imgX, lineMovement[0]), gg_shakeValue(imgY, lineMovement[1]));
		points.push([imgX, imgY]);
		// fill in the plot
		for (var p = 0; p < points.length; p++) {
			var theX = points[p][0];
			var theY = points[p][1];
			var theSpot = theY;
			var theMax = canvasSize[1];
			var yIncrement = data['imageObj'].height;
			var spotIncrement = yIncrement;
			if (flip) {
				yIncrement = -1 * yIncrement;
				theSpot = canvasSize[1] - theSpot;
			}
			else {
				theMax -= bottomSpace;
			}
			/*if (fill) {
				yIncrement /= 3;
				spotIncrement /= 3;
			}*/
			theY += yIncrement;

			while (theSpot < theMax) {
				//if (!fill) {
					theX = gg_shakeValue(theX, interiorMovement[0]);
					theY = gg_shakeValue(theY, interiorMovement[1]);
				//}
				context.drawImage(data['imageObj'], theX, theY);
				theY += yIncrement;
				theSpot += spotIncrement;
			}
		}
	}
}

// main function called by library users
function graphicGraph(canvas, data, options) {
	error = false;
	var canvasElement = document.getElementById(canvas);
	if (!canvasElement) {
		console.log("No canvas provided!");
		error = true;
	}
	if (!data) {
		console.log("No data provided!");
		error = true;
	}
	
	var gg_canvasSize = [canvasElement.width, canvasElement.height];

	var allData = [];
	for (var dataObj = 0; dataObj < data.length; dataObj++) {
		allData.push(data[dataObj]['data']);
	}

	if (!error) {
		var ctx = canvasElement.getContext("2d");
	
		for (var dataObj = 0; dataObj < data.length; dataObj++) {
			//console.log(data[dataObj]);
			var theImageURL = data[dataObj]['image'];
			var theImage = new Image();
			theImage.gg_dataNumber = dataObj;
			data[theImage.gg_dataNumber]['imageLoaded'] = false;
			data[theImage.gg_dataNumber]['imageLoadError'] = false;
			// image onload event
			theImage.onload = function() {
				data[this.gg_dataNumber]['imageLoaded'] = true;
				data[this.gg_dataNumber]['imageObj'] = this;
				
				gg_plotData(ctx, data[this.gg_dataNumber], gg_canvasSize);
			};
			// image onerror event
			theImage.onerror = function() {
				data[this.gg_dataNumber]['imageLoaded'] = true;
				data[this.gg_dataNumber]['imageObj'] = null;
				data[this.gg_dataNumber]['imageLoadError'] = true;
				
				gg_plotData(ctx, data[theImage.gg_dataNumber], gg_canvasSize);
			};
			theImage.src = theImageURL;
		}
	}
}
