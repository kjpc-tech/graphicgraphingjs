graphicGraphingJS Library   
Kyle Johnson - fredrik.johnson.kj@gmail.com   
License: GNU LGPL v3   
* * *
JavaScript graphing library that makes graphical plots.
* * *
Dependencies:   
 - HTML5 canvas support.   
   
See the [Wiki](https://bitbucket.org/fredriksapps/graphicgraphingjs/wiki/) for more information.   
   
Example Graph - this shows grass growth vs precipitation:   
![Graph Screenshot](example_website_graph.png)